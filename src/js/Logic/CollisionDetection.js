import { indexesToKey, keyToIndexes } from "../Utils/CoordinatesUtils";



class Collision {

    collideWalls(xElements, yElements) {
        this.xElements = xElements;
        this.yElements = yElements;
    }

    collide(object, collideTileMap) {
        if(object.xIndex < 0 || object.xIndex > this.xElements 
                || object.yIndex < 0 || object.yIndex > this.yElements
                || collideTileMap.get(indexesToKey(object.xIndex, object.yIndex)) === true) {
            return true;
        }
        return false
    }

}

const collision = new Collision();
export default collision;