import { rand } from "../../Utils/MathUtils";

class FoodManager {

    constructor() {
        this.food = null;
    }

    generateFood(allowedFieldsArray) {
        var index = rand(0, allowedFieldsArray.length);
        this.food = {xIndex: allowedFieldsArray[index].xIndex, yIndex: allowedFieldsArray[index].yIndex};
    }

    isFoodEatenBySnake(snake) {
        if(this.food.xIndex === snake.getHead().xIndex 
                && this.food.yIndex === snake.getHead().yIndex) {
            return true;
        }
        return false;
    }

    getFood() {
        return this.food;
    }

}

var foodManager = new FoodManager();
export default foodManager;
