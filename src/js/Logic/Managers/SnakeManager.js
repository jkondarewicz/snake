import Collision from "../CollisionDetection";

export default class SnakeManager {

    constructor(snake) {
        this.snake = snake;
    }

    collide(direction, collideTilesMap = new Map()) {
        var head = this.snake.getHead();
        var xIndex = head.xIndex + direction.horizontal;
        var yIndex = head.yIndex + direction.vertical;
        return Collision.collide({xIndex, yIndex}, collideTilesMap);
    }

    move(direction) {
        this.moveBody();
        var head = this.snake.getHead();
        head.xIndex += direction.horizontal
        head.yIndex += direction.vertical;
    } 

    moveBody() {
        this.appendBodyIfFoodWasEaten();
        for(var i = this.snake.getBody().length - 1; i >= 1; i--) {
            this.snake.getBody()[i].xIndex = this.snake.getBody()[i - 1].xIndex;
            this.snake.getBody()[i].yIndex = this.snake.getBody()[i - 1].yIndex;
            this.snake.getBody()[i].eatingFood = this.snake.getBody()[i - 1].eatingFood;
        }
        this.snake.getBody()[0].xIndex = this.snake.getHead().xIndex;
        this.snake.getBody()[0].yIndex = this.snake.getHead().yIndex;
        this.snake.getBody()[0].eatingFood = this.snake.getHead().eatingFood;
        this.snake.getHead().eatingFood = false;
        
    }

    appendBodyIfFoodWasEaten() {
        var body = this.snake.getBody();
        if(body[body.length - 1].eatingFood) {
            body[body.length - 1].eatingFood = false;
            this.snake.appendBody(body[body.length - 1].xIndex, body[body.length - 1].yIndex);
            
        }
    }

    snakeAteFood() {
        this.snake.getHead().eatingFood = true;
    }

}
