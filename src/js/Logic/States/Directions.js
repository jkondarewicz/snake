
export const DIRECTIONS = {
    LEFT: {
        vertical: 0,
        horizontal: -1,
        value: -1
    },
    RIGHT: {
        vertical: 0,
        horizontal: 1,
        value: 1
    },
    UP: {
        vertical: -1,
        horizontal: 0,
        value: -2
    },
    DOWN: {
        vertical: 1,
        horizontal: 0,
        value: 2
    }
};