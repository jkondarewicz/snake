
const listeners = new Map();

export function initialize() {
    window.addEventListener("keydown", function(e) {
        const keyCode = e.keyCode;
        if(listeners.has(keyCode)) {
            listeners.get(e.keyCode).forEach(function(listener) {
                listener();
            });
        }
    });
}

export function registerListener(keyCode, func) {
    if(!listeners.has(keyCode)) {
        listeners.set(keyCode, []);
    }
    listeners.get(keyCode).push(func);
}
