import { indexesToKey } from "../../Utils/CoordinatesUtils";

export default class Snake {

    constructor(xIndex, yIndex, bodyLength = 3) {
        this.head = {xIndex, yIndex, eatingFood: false};
        this.body = [];
        for(var i = 0; i < bodyLength; i++) {
            this.body.push({xIndex, yIndex: yIndex + 1 + i, eatingFood: false});
        }
    }

    getHead() {
        return this.head;
    }

    getBody() {
        return this.body;
    }

    getSnakeAsArray() {
        return [this.head, ...this.body];
    }

    getSnakesAsMap() {
        const arrSnake = this.getSnakeAsArray();
        const mapSnake = new Map();
        arrSnake.forEach(snake => mapSnake.set(indexesToKey(snake.xIndex, snake.yIndex), true));
        return mapSnake;
    }

    appendBody(xIndex, yIndex) {
        this.body.push({xIndex, yIndex, eatingFood: false});
    }

}
