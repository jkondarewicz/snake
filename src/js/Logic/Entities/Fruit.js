
export default class Fruit {

    constructor(xIndex, yIndex) {
        this.xIndex = xIndex;
        this.yIndex = yIndex;
    }

    getXIndex() {
        return this.xIndex;
    }

    getYIndex() {
        return this.yIndex;
    }

}
