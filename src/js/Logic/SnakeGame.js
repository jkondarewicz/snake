import Snake from "./Entities/Snake";
import SnakeManager from "./Managers/SnakeManager";
import {initialize as initKeyboard, registerListener as registerKeyboardListener} from './Listeners/KeyboardListener';
import { DIRECTIONS } from './States/Directions';
import FoodManager from "./Managers/FoodManager";
import { indexesToKey } from "../Utils/CoordinatesUtils";

const UP_KEY_CODE = 38;
const DOWN_KEY_CODE = 40;
const LEFT_KEY_CODE = 37;
const RIGHT_KEY_CODE = 39;

class SnakeGame {

    constructor() {
        const that = this;
        initKeyboard();
        registerKeyboardListener(UP_KEY_CODE, function() {that.setDirection(DIRECTIONS.UP)});
        registerKeyboardListener(DOWN_KEY_CODE, function() {that.setDirection(DIRECTIONS.DOWN)});
        registerKeyboardListener(LEFT_KEY_CODE, function() {that.setDirection(DIRECTIONS.LEFT)});
        registerKeyboardListener(RIGHT_KEY_CODE, function() {that.setDirection(DIRECTIONS.RIGHT)});
        this.directionSet = false;
        this.direction = DIRECTIONS.UP;
        this.playerSnake = new Snake(10, 10);
        this.playerSnakeManager = new SnakeManager(this.playerSnake);
        this.lose = false;
    }

    initializeBoardTiles(boardTiles) {
        this.boardTiles = boardTiles;
        this.generateFood();
    }

    getBoardTilesAsMap() {
        const mapBoardTiles = new Map();
        this.boardTiles.forEach(boardTile => mapBoardTiles.set(indexesToKey(boardTile.xIndex, boardTile.yIndex), {xIndex: boardTile.xIndex, yIndex: boardTile.yIndex}));
        return mapBoardTiles;
    }

    setDirection(direction) {
        if(!this.directionSet)
            this.direction = getFixDirection(direction, this.direction);
        this.directionSet = true;
    }

    update(deltaTime) {
        this.directionSet = false;
        this.lose = this.playerSnakeManager.collide(this.direction, this.playerSnake.getSnakesAsMap());
        if(!this.lose) {
            this.playerSnakeManager.move(this.direction);
            var foodEaten = FoodManager.isFoodEatenBySnake(this.playerSnake);
            if(foodEaten) {
                this.generateFood();
                this.playerSnakeManager.snakeAteFood();
            }
        }
    }

    generateFood() {
        const boardTilesMap = this.getBoardTilesAsMap();
        const snakeAsMap = this.playerSnake.getSnakesAsMap();
        snakeAsMap.forEach((value, key) => boardTilesMap.delete(key));
        const boardTilesAsArray = [];
        boardTilesMap.forEach((value, key) => boardTilesAsArray.push(value));
        FoodManager.generateFood(boardTilesAsArray);
    }

    getPlayerSnake() {
        return this.playerSnake;
    }

    getFood() {
        return FoodManager.getFood();
    }

}

function getFixDirection(newDirection, currentDirection) {
    if(newDirection.value * -1 === currentDirection.value) {
        return currentDirection;
    }
    return newDirection;
}

const snakeGame = new SnakeGame();
export default snakeGame;
