import context from '../Utils/CanvasContext';
import { indexToWorldPosition } from '../Utils/CoordinatesUtils';
import { SINGLE_ELEMENT_SIZE, BORDER } from '../canvas';

function convertSnakeEntityToQuad(snakeEntity) {
    const border = snakeEntity.eatingFood ? BORDER * -2 : BORDER;
    return {
        x: indexToWorldPosition(snakeEntity.xIndex) + border,
        y: indexToWorldPosition(snakeEntity.yIndex) + border,
        width: SINGLE_ELEMENT_SIZE - border * 2,
        height: SINGLE_ELEMENT_SIZE - border * 2,
    };
}

class SnakeRenderer {

    render(snake, snakeColor = 'green') {
        context().fillStyle = snakeColor;
        var snakeEntities = [snake.getHead(), ...snake.getBody()];
        for(var i = 0; i < snakeEntities.length; i++) {
            const snakeField = convertSnakeEntityToQuad(snakeEntities[i]);
            context().fillRect(snakeField.x, snakeField.y, 
                snakeField.width, snakeField.height);
            if(snakeEntities[i].eatingFood) {
                context().fillStyle = 'red';  
                const foodInsideSnake = convertSnakeEntityToQuad({...snakeEntities[i], eatingFood: false});
                context().fillRect(foodInsideSnake.x, foodInsideSnake.y, 
                    foodInsideSnake.width, foodInsideSnake.height);
                context().fillStyle = snakeColor;
            }
        }
    }

}

const snakeRenderer = new SnakeRenderer();
export default snakeRenderer;