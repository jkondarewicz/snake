import context from '../Utils/CanvasContext';
import { BORDER, SINGLE_ELEMENT_SIZE } from '../canvas';
import { indexToWorldPosition } from '../Utils/CoordinatesUtils';

function convertFoodEntityToQuad(foodEntity) {
    return {
        x: indexToWorldPosition(foodEntity.xIndex) + BORDER,
        y: indexToWorldPosition(foodEntity.yIndex) + BORDER,
        width: SINGLE_ELEMENT_SIZE - BORDER * 2,
        height: SINGLE_ELEMENT_SIZE - BORDER * 2,
        
    };
}

class FoodEntityRenderer {

    render(food) {
        food = convertFoodEntityToQuad(food);
        context().fillStyle = "red";
        context().fillRect(food.x, food.y, food.width, food.height);
    }

}

const foodEntityRenderer = new FoodEntityRenderer();
export default foodEntityRenderer;
