import context from '../Utils/CanvasContext';
import { BORDER } from '../canvas';

class BoardTilesRenderer {

    render(emptyFields) {
        context().fillStyle = "black";
        for(var i = 0; i < emptyFields.length; i++) {
            const emptyField = emptyFields[i];
            context().fillRect(emptyField.x + BORDER, emptyField.y + BORDER, 
                emptyField.width - BORDER * 2, emptyField.height - BORDER * 2);
        }
    }

}

const boardTilesRenderer = new BoardTilesRenderer();
export default boardTilesRenderer;
