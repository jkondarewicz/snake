export const DEFAULT_WIDTH = window.innerWidth - 10;
export const DEFAULT_HEIGHT = window.innerHeight - 10;
//export const DEFAULT_WIDTH = 1360;
//export const DEFAULT_HEIGHT = 768;

export const CANVAS_ID = "my-canvas";
export const SINGLE_ELEMENT_SIZE = 30;
export const BORDER = 2;