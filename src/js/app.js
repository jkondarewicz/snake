import {DEFAULT_WIDTH, DEFAULT_HEIGHT, CANVAS_ID} from './canvas';
import Game from "./Game";

const canvas = document.createElement("canvas");
canvas.setAttribute("width", DEFAULT_WIDTH);
canvas.setAttribute("height", DEFAULT_HEIGHT);
canvas.setAttribute("id", CANVAS_ID);
document.body.appendChild(canvas);
new Game().init();