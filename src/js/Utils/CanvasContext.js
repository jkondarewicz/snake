import {CANVAS_ID} from '../canvas';

var context = null;

export default function getContext() {
	if(context === null) {
		const canvas = document.getElementById(CANVAS_ID);
		context = canvas.getContext('2d');
	}
	return context;
}
