import {worldPositionToTndex, indexToWorldPosition} from './CoordinatesUtils';

export default function generateBoardTiles(xElements, yElements, singleElementSize) {
    var tiles = [];
    for(var i = 0; i < xElements * singleElementSize; i += singleElementSize) {
        for(var j = 0; j < yElements * singleElementSize; j += singleElementSize) {
            tiles.push({
                x: indexToWorldPosition(worldPositionToTndex(i)),
                y: indexToWorldPosition(worldPositionToTndex(j)),
                width: singleElementSize,
                height: singleElementSize
            });
        }
    }
    return tiles;
}
