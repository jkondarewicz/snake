import {SINGLE_ELEMENT_SIZE} from '../canvas';

export function worldPositionToTndex(worldPosition) {
	return parseInt(worldPosition / SINGLE_ELEMENT_SIZE);
}

export function indexToWorldPosition(index) {
	return parseInt(index * SINGLE_ELEMENT_SIZE);
}

export function indexesToKey(xIndex, yIndex) {
	return xIndex + ';' + yIndex;
}

export function keyToIndexes(key) {
	const indexes = key.split(';');
	return {
		xIndex: parseInt(indexes[0]),
		yIndex: parseInt(indexes[1])
	};
}


