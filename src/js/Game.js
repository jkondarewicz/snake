import time from './Utils/TimeUtils';
import generateBoardTiles from './Utils/BoardTileUtils';
import {SINGLE_ELEMENT_SIZE, DEFAULT_HEIGHT, DEFAULT_WIDTH} from './canvas'
import BoardRenderer from './Renderers/BoardTilesRenderer';
import context from './Utils/CanvasContext'
import SnakeRenderer from './Renderers/SnakeRenderer';
import SnakeGame from './Logic/SnakeGame';
import Collision from './Logic/CollisionDetection';
import { worldPositionToTndex } from './Utils/CoordinatesUtils';
import FoodRenderer from './Renderers/FoodRenderer';

const MS_PER_UPDATE = 60;

export default class Game {
	
	init() {
		Collision.collideWalls(19, 19);
		this.boardTiles = generateBoardTiles(20, 20, SINGLE_ELEMENT_SIZE);
		SnakeGame.initializeBoardTiles(this.boardTiles.map(boardTile => {
			return {
				xIndex: worldPositionToTndex(boardTile.x),
				yIndex: worldPositionToTndex(boardTile.y)
			}
		}));
		this.currentTime = 0;
		this.previousTime = time();
		this.elapsed = 0;
		this.lag = 0;
		this.gameLoop = this.gameLoop.bind(this);
		this.gameLoop();
	}
	
	update(deltaTime) {
		SnakeGame.update(deltaTime);
	}
	
	render() {
		context().clearRect(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
		BoardRenderer.render(this.boardTiles);
		SnakeRenderer.render(SnakeGame.getPlayerSnake());
		FoodRenderer.render(SnakeGame.getFood());
	}
	
	gameLoop() {
		window.requestAnimationFrame(this.gameLoop);

		this.currentTime = time();
		this.elapsed = this.currentTime - this.previousTime;
		this.previousTime = this.currentTime;
		this.lag += this.elapsed;
		while(this.lag >= MS_PER_UPDATE) {
			this.update(this.lag / MS_PER_UPDATE);
			this.lag -= MS_PER_UPDATE;
		}
		this.render();
	}
	
}
